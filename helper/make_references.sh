#!/usr/bin/env bash

PRESENTATION=$1
PUBMED2BIBTEX=$2
REFERENCES=$3

PMIDS=$(grep -o "pmid[0-9]*" $PRESENTATION | sort -u | sed 's/pmid//' | tr '\r\n' ',')
curl -s -X GET "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=${PMIDS%?}&retmode=xml" > temp.xml
xsltproc $PUBMED2BIBTEX temp.xml > $REFERENCES
